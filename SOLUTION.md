# Part 1: Answers to questions 

## Qs 1

Seqence of events:

1. `setTimeout` is called.  
2. This starts a timer that will run `setTimeout`'s first argument after the number of milliseconds specified by its second argument has passed.
3. `console.log("2")` is called and this outputs `2` to the console.
4. The timer reaches `100` after which `console.log("1")` runs. This outputs `1` to the console. 

## Qs 2 

Sequence of events:

1. Set `d` equal to `0`.
2. Call `foo` with `d` as argument.
3. If `d` is less than `10`, suspend current execution of `foo` and add it to the top of the stack, then increment `d` by `1` and repeat from step **2**.
4. Run final line of `foo`. This logs the call's value of `d` to the console
5. For any calls that have been suspended, remove them from the top of the stack in turn and resume. This runs step **4** for each. 

## Qs 3 

The `||` operator returns the first truthy value reading left to ride.  
This means if `0` or `''` (both of which are `falsy`) are provided to `foo`, then `d` will still be reset to `5` for the response.  
To avoid this use a default parameter:

```js
    function foo(d = 5) {
      console.log(d);
    }
```

## Qs 4 

`bar` is a function that returns `a + b`.  
`a` was set to `1` when bar was declared.  
So calling `bar` with `b` equal to `2` returns `1 + 2` which is `3`.  
`3` is therefore logged to the console.  

## Qs 5

`double` calls `done` with an argument of twice `a` after `100` milliseconds.  

# Part 2:  The geo search server

Before running the server, install `node_modules` by running from project root:

`npm install`

Then to start the server run:

`npm start`

To return single search results, in a browser go to:

`http:localhost:3000/locations?q=fuzzyMatchString`

Where `fuzzyMatchString` is a search term at least 2 characters in length.

Results will be returned as an array in `json` format.

# Part 3: The search form 

To install dependencies then start the server if not already done so, from project root run `npm install` then `npm start`

The search form may now be used. In a browser go to:

`http:localhost:3000/`

Type into the search box and the results will be displayed beneath.


# Notes 

## Data errors

The data supplied had some errors. A space had to be replaced with a tab in the header row. Other anomolies were left as is.

## Development Mode 

In development the server was run with `npm run dev` instead of `npm start` for hot reloading.


