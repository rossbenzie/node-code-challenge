var express = require('express');
const router = express.Router()
const sqlite3 = require('sqlite3').verbose();

router.get('/', (req, res) => {
  let db = new sqlite3.Database('./GB.db', sqlite3.OPEN_READWRITE, err => {
    if (err) {
      console.log(err.message);
      const message = 'Database error'
      res.setStatus(500)
      res.send({ message })
    }
    console.log('Connected to the GB database.');
  });

  const { q } = req.query
  const srchString = q.replace(/[^a-zA-Z\s]/g, '')

  if (!srchString.length || srchString.length < 2) {
    db.close()
    res.send({})
  } else {
    const sql = `SELECT name FROM location WHERE name LIKE '${srchString}%'`
    db.all(sql, [], (err, rows) => {
      if (err) {
        db.close()
        const message = 'Database error'
        res.setStatus(500)
        res.send({ message });
      } else if (rows) {
        const result = rows.map(row => row.name)
        db.close();
        res.send(result)
      }
    });
  }
})

module.exports = router;