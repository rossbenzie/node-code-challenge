const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const path = require('path')
const indexRouter = require('./routes/index')
const locationsRouter = require('./routes/locations')

const app = express()
app.use(logger('dev'))

app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/locations', locationsRouter)

app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  res.send("Page not found")
});

module.exports = app
