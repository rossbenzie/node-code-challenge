#!/bin/sh
echo "Setting up database..."
sqlite3 <<EOF
.mode tabs 
.import ./data/GB.tsv location
.save GB.db
.exit 
EOF
echo
echo "Database setup complete."
echo