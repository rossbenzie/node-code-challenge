const str = document.getElementById('search-string')
const res = document.getElementById('results')

str.addEventListener('input', () => {
  if (str.value) {
    const q = str.value
    fetch(`http://localhost:3000/locations?q=${q}`)
      .then(response => {
        return response.json()
      })
      .then(items => {
        const list = items && items.length
          ? items.reduce((list, item) => list += `<li>${item}</li>`, '')
          : '<li>No results to show</li>'
        res.innerHTML = list
      })
      .catch(err => {
        console.log(err)
        res.innerHTML = '<li>Error retrieving results</li>'
      })
  }
})
